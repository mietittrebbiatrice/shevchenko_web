package homefinance.dao.repository;

import homefinance.dao.*;
import homefinance.dao.model.*;

import java.sql.*;
import java.util.ArrayList;

public class CurrencyRepository implements Repository<CurrencyModel> {

    private ConnectionSupplier connectionSupplier;

    public CurrencyRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public CurrencyModel save(CurrencyModel model) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                ("INSERT INTO currencyModel_tbl (name, code, symbol) VALUES (?,?,?)",Statement.RETURN_GENERATED_KEYS)){

                preparedStatement.setString(1, model.getName());
                preparedStatement.setString(2, model.getCode());
                preparedStatement.setString(3, model.getSymbol());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in save CurrencyRepository", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save CurrencyRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM currencymodel_tbl WHERE id = " + id)){

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in remove CurrencyRepository", e);
            }
        } catch (SQLException e){
            throw new HomeFinanceException("Error in remove CurrencyRepository", e);
        }
    }

    @Override
    public CurrencyModel findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                ("SELECT * FROM currencymodel_tbl WHERE id = " + "'" + id + "'")) {

                CurrencyModel currencyModel = new CurrencyModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                currencyModel.setId(resultSet.getInt(1));
                currencyModel.setName(resultSet.getString(2));
                currencyModel.setCode(resultSet.getString(3));
                currencyModel.setSymbol(resultSet.getString(4));
                return currencyModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID CurrencyRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID CurrencyRepository", e);
        }
    }
    public CurrencyModel findByName(String name) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl WHERE name = " + "'" + name + "'")) {

                CurrencyModel currencyModel = new CurrencyModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                currencyModel.setId(resultSet.getInt(1));
                currencyModel.setName(resultSet.getString(2));
                currencyModel.setCode(resultSet.getString(3));
                currencyModel.setSymbol(resultSet.getString(4));
                return currencyModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByName CurrencyRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByName CurrencyRepository", e);
        }
    }

    @Override
    public void update(CurrencyModel model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE currencymodel_tbl SET name = " + "'" + model.getName() + "'" +
                                                                         ", code = " + "'" + model.getCode() + "'" +
                                                                         ", symbol = " + "'" + model.getSymbol() + "'" +
                                                                         "WHERE ID = " + model.getId()))  {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in update CurrencyRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in update CurrencyRepository", e);
        }
    }

    @Override
    public ArrayList<CurrencyModel> findAll() {
        try(Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM currencymodel_tbl")) {

                ArrayList<CurrencyModel> currencyModels = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    CurrencyModel currencyModel = new CurrencyModel();
                    currencyModel.setId(resultSet.getInt(1));
                    currencyModel.setName(resultSet.getString(2));
                    currencyModel.setCode(resultSet.getString(3));
                    currencyModel.setSymbol(resultSet.getString(4));
                    currencyModels.add(currencyModel);
                }
                return currencyModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll CurrencyRepository", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in findAll CurrencyRepository", e);
        }
    }
}