package homefinance.dao.repository;

//import ru.geekfactory.homefinance.dao.*;

import homefinance.dao.HomeFinanceException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSupplier {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/homefinance";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "admin";

    public Connection connection() {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            conn.setAutoCommit(false);

            return conn;
        } catch (SQLException e) {
            throw  new HomeFinanceException("Error in ConnectionSupplier", e);
        }
    }
}